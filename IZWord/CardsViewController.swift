//
//  CardsViewController.swift
//  IZWord
//
//  Created by lvmtam on 9/13/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import UIKit

class CardsViewController : BaseViewController {
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var indicatorLabel: UILabel!
    @IBOutlet weak var guidelineView: UIView!
    
    var lesson: Lesson? {
        didSet {
            if let notNilLesson = self.lesson {
                navigationItem.title = notNilLesson.name
                cards = notNilLesson.cards?.array as! [Card]
            }
        }
    }
    
    var cards = [Card]() {
        didSet {
            if !cards.isEmpty {
                updateUI()
            }
            collectionview?.reloadData()
            guidelineView?.hidden = cards.isEmpty ? false : true
        }
    }
    
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lesson?.addObserver(self, forKeyPath: "cards", options: NSKeyValueObservingOptions.New, context: nil)
    }
    
    
    deinit {
        cards.removeAll()
        lesson?.removeObserver(self, forKeyPath: "cards")
        lesson = nil
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidDisappear(animated)
        updateUI()
    }
    
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == "cards", let newValue = change?[NSKeyValueChangeNewKey] as? NSOrderedSet {
            self.cards = newValue.array as! [Card]
        }
    }

    private func updateUI() {
        if collectionview != nil {
            let initialPinchPoint = CGPointMake(collectionview.center.x + collectionview.contentOffset.x, collectionview.center.y + collectionview.contentOffset.y);
            let indexpath = collectionview.indexPathForItemAtPoint(initialPinchPoint)
            let index = indexpath == nil ? 0 : indexpath!.row + 1
            indicatorLabel?.text = "\(index) OF \(cards.count)"
        }
    }
    
    
    // MARK: - UI Events
    @IBAction func addCardButtonDidPressed(sender: UIButton) {}
    
    
    
    // MARK: - Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destination = segue.destinationViewController as? CardAddingViewController {
            if let ppc = destination.popoverPresentationController {
                ppc.delegate = self
            }
            destination.lesson = lesson
        }
    }
}



extension CardsViewController : UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cards.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CardCell", forIndexPath: indexPath) as! CardCell
        cell.card = cards[indexPath.row]
        return cell
    }
}



extension CardsViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            updateUI()
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        updateUI()
    }
}



extension CardsViewController : UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
}




