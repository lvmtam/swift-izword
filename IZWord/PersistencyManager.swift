//
//  PersistencyManager.swift
//  IZWord
//
//  Created by lvmtam on 9/13/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import Foundation
import CoreData
import UIKit



enum PersistencyManagerError: ErrorType {
    case LessonNameCollision
    case UnknowError
    
    var description: String {
        switch self {
        case .LessonNameCollision:
            return "Lesson name be collision"
        case .UnknowError:
            return "Unknow error"
        }
    }
}



class PersistencyManager : NSObject {
    
    private var lessons = [Lesson]()
    private var context: NSManagedObjectContext {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        return appDelegate.managedObjectContext
    }
    
    
    // MARK: - PARSING
    func loadLessonsFromJSON() {
        let path = NSBundle.mainBundle().pathForResource("kanjiv2", ofType: "json")
        let data = NSData(contentsOfFile: path!)
        let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableLeaves) as! NSDictionary

        if let lessonsJSON = json.valueForKey("lessons") as? [NSDictionary] {
            for dictionary in lessonsJSON {
                Lesson.lessonWithInfo(dictionary, managedObjectContext: context)
            }
        }
        // save to database
        do {
            try context.save()
        } catch {
            print("fatal : can not stored into core database")
        }
    }
    
    
    func removeAllData() {
        let request = NSFetchRequest(entityName: "Lesson")
        do {
            if let matched = try context.executeFetchRequest(request) as? [Lesson] {
                for lesson in matched {
                    context.deleteObject(lesson)
                }
                try context.save()
            }
        } catch {
            print("fatal : fetch all lesson failure")
        }
    }
    

    // MARK: public API
    func getLessons() -> [Lesson] {
        let request = NSFetchRequest(entityName: "Lesson")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        var result = [Lesson]()
        do {
            result = try context.executeFetchRequest(request) as! [Lesson]
        } catch {
            print("fatal : can not fetch all lesson")
        }
        return result
    }
    
    
    func getLesson(name: String) -> Lesson? {
        let request = NSFetchRequest(entityName: "Lesson")
        request.predicate = NSPredicate(format: "name = %@", name)
        var result: Lesson?
        do {
            let fetchedResult = try context.executeFetchRequest(request)
            if let lesson = fetchedResult.first as? Lesson {
                result = lesson
            }
        } catch {
            print("fatal : can not fetch lesson \(name)")
        }
        return result
    }
    
    
    func addLesson(name: String) throws -> Lesson {
        
        if hasStoredLesson(name) {
            throw PersistencyManagerError.LessonNameCollision
        }
        
        let lesson = Lesson(context: context)
        lesson.name = name
        try context.save()
        
        return lesson
    }
    
    
    func deleteLesson(lesson: Lesson) {
        context.deleteObject(lesson)
        do {
            try context.save()
        } catch {
            print("fatal : can not delete lesson \(lesson.name)")
        }
    }
    
    
    private func hasStoredLesson(name: String) -> Bool {
        return self.getLesson(name) != nil ? true : false
    }
    
    
    
    // MARK: - Card API
    
    func addCard(kanji: String, meaning: String, romaji: String?, forLesson lesson: Lesson) -> Card? {
        
        let card = Card(kanji: kanji, meaning: meaning, romaji: romaji, context: context)
        
        lesson.addCard(card)
        
        do {
            try context.save()
            return card
        } catch {
            return nil
        }
    }
}







