//
//  Lesson+CoreDataProperties.swift
//  IZWord
//
//  Created by lvmtam on 9/16/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Lesson {

    @NSManaged var id: NSNumber?
    @NSManaged var name: String?
    @NSManaged var cards: NSOrderedSet?

}
