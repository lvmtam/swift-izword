//
//  CardsLayout.swift
//  IZWord
//
//  Created by lvmtam on 9/13/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import UIKit



class CardLayoutAttributes: UICollectionViewLayoutAttributes {
    
}

class CardsLayout : UICollectionViewFlowLayout {
    
    private let PageWidth: CGFloat = 245
    private let PageHeight: CGFloat = 365
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        scrollDirection = UICollectionViewScrollDirection.Horizontal
        itemSize = CGSizeMake(PageWidth, PageHeight)
        minimumInteritemSpacing = 10
    }
    
    
    override func prepareLayout() {
        super.prepareLayout()
        
        collectionView?.decelerationRate = UIScrollViewDecelerationRateFast
        
        collectionView?.contentInset = UIEdgeInsets(
            top: 0,
            left: collectionView!.bounds.width/2 - PageWidth/2,
            bottom: 0,
            right: collectionView!.bounds.width/2 - PageWidth/2)
    }
    
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let array = super.layoutAttributesForElementsInRect(rect) ?? []
        
        for attributes in array {
            let frame = attributes.frame
            let distance = abs(collectionView!.contentOffset.x + collectionView!.contentInset.left - frame.origin.x)
            let scale = 1.0 * min(max(1 - distance / (collectionView!.bounds.width), 0.75), 1)
            attributes.transform = CGAffineTransformMakeScale(scale, scale)
        }
    
        return array
    }
    
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        return true
    }
    
    
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        var newOffset = CGPoint()
        let layout = collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        let width = layout.itemSize.width + layout.minimumLineSpacing
        var offset = proposedContentOffset.x + collectionView!.contentInset.left
        
        if velocity.x > 0 {
            //ceil returns next biggest number
            offset = width * ceil(offset / width)
        } else if velocity.x == 0 {
            //rounds the argument
            offset = width * round(offset / width)
        } else if velocity.x < 0 {
            //removes decimal part of argument
            offset = width * floor(offset / width)
        }
        newOffset.x = offset - collectionView!.contentInset.left
        newOffset.y = proposedContentOffset.y //y will always be the same...
        return newOffset
    }
}