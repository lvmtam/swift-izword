//
//  LibraryAPI.swift
//  IZWord
//
//  Created by lvmtam on 9/13/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import Foundation


class LibraryAPI {
    
    private let persistencyManager = PersistencyManager()
    
    class var sharedInstance: LibraryAPI {
        struct Singleton {
            static let instance = LibraryAPI()
        }
        return Singleton.instance
    }
    
    
    
    // MARK: - Synchronization
    func synchronizeLocalJson() {
        persistencyManager.removeAllData()
        persistencyManager.loadLessonsFromJSON()
    }
    
    
    
    // MARK: - Lesson API
    func getLessons() -> [Lesson] {
        return persistencyManager.getLessons()
    }
    
    func addLesson(name: String) throws -> Lesson {
        return try persistencyManager.addLesson(name)
    }
    
    func deleteLesson(lesson: Lesson) {
        persistencyManager.deleteLesson(lesson)
    }
    
    
    // MARK: - Card API
    func addCard(kanji: String, meaning: String, romaji: String?, forLesson lesson: Lesson) -> Card? {
        return persistencyManager.addCard(kanji, meaning: meaning, romaji: romaji, forLesson: lesson)
    }
}