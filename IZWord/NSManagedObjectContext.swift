//
//  NSManagedObjectContext.swift
//  IZWord
//
//  Created by lvmtam on 9/14/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
    
    public func insertObject<T: NSManagedObject>() -> T {
        guard let object = NSEntityDescription.insertNewObjectForEntityForName(T.entityName(), inManagedObjectContext: self) as? T
            else { fatalError("Invalid Core Data Model.") }
        return object;
    }
}