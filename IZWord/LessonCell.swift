//
//  LessonCell.swift
//  IZWord
//
//  Created by lvmtam on 9/13/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import UIKit

class LessonCell : UITableViewCell {
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var indicator: UIView!
    
    var lesson: Lesson? {
        didSet {
            if let name = lesson?.name, cards = lesson?.cards {
                caption.text = name
                detail.text = "\(cards.count) Cards"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = false
        layer.shadowOffset = CGSizeMake(0, 1);
        layer.shadowRadius = 2;
        layer.shadowOpacity = 0.35;
    }
}