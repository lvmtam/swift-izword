//
//  Card+CoreDataProperties.swift
//  IZWord
//
//  Created by lvmtam on 9/16/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Card {

    @NSManaged var kanji: String?
    @NSManaged var meaning: String?
    @NSManaged var romaji: String?
    @NSManaged var lesson: Lesson?

}
