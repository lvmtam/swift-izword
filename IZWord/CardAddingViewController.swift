//
//  CardAddingViewController.swift
//  IZWord
//
//  Created by lvmtam on 9/15/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import UIKit

class CardAddingViewController : BaseViewController {
    
    @IBOutlet weak var kanjiLabel: UILabel!
    @IBOutlet weak var romajiLabel: UILabel!
    @IBOutlet weak var meaningLabel: UILabel!
    @IBOutlet weak var kanjiTextField: UITextField!
    @IBOutlet weak var romajiTextField: UITextField!
    @IBOutlet weak var meaningTextField: UITextField!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var lesson: Lesson!
    
    override var preferredContentSize: CGSize {
        get {
            let w = super.preferredContentSize.width
            let h: CGFloat = 300
            return CGSizeMake(w, h)
        }
        set { super.preferredContentSize = newValue }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard:")))
        
        kanjiTextField.addTarget(self, action: Selector("updateUI"), forControlEvents: UIControlEvents.EditingChanged)
        meaningTextField.addTarget(self, action: Selector("updateUI"), forControlEvents: UIControlEvents.EditingChanged)
        
        kanjiTextField.becomeFirstResponder()
    }
    
    func dismissKeyboard(sender: AnyObject) {
        view.endEditing(true)
    }
    
    
    func updateUI() {
        if kanjiTextField.text?.characters.count > 0 && meaningTextField.text?.characters.count > 0 {
            createButton.enabled = true
        } else {
            createButton.enabled = false
        }
    }
    
    
    @IBAction func addButtonDidPressed(sender: UIButton) {
        
        guard let kanji = kanjiTextField.text where kanjiTextField.text?.characters.count > 0 else {
            showError("The kanji input is required")
            return
        }
        guard let meaning = meaningTextField.text where meaningTextField.text?.characters.count > 0 else {
            showError("The meaning input is required")
            return
        }
        
        if LibraryAPI.sharedInstance.addCard(kanji, meaning: meaning, romaji: romajiTextField.text, forLesson: lesson) != nil {
            dismissViewControllerAnimated(true, completion: nil)
        } else {
            showError("fatal: can not add new card into database")
        }
    }
    
    @IBAction func cancelButtonDidPressed(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
