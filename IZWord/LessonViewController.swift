//
//  LessonViewController.swift
//  IZWord
//
//  Created by lvmtam on 9/12/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import UIKit


class LessonViewController : BaseViewController {
    
    // Outlets
    @IBOutlet weak var guidelineView: UIView!
    @IBOutlet weak var tableview: UITableView!
    
    // Properties
    var selectedIndexPath: NSIndexPath?
    var lessons: [Lesson] = [] {
        didSet {
            if lessons.isEmpty {
                guidelineView.hidden = false
                tableview.hidden = true
            } else {
                guidelineView.hidden = true
                tableview.hidden = false
            }
        }
    }
  
  
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lessons = LibraryAPI.sharedInstance.getLessons()
        tableview.tableFooterView = UIView.init(frame: CGRectZero)
        
        let gesture = UILongPressGestureRecognizer(target: self, action: Selector("longPress:"))
        tableview.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableview.reloadData()
    }
    
    
    // MARK: - Prepare for segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let cvc = segue.destinationViewController as? CardsViewController {
            if let indexpath = tableview.indexPathForCell(sender as! UITableViewCell) {
                cvc.lesson = lessons[indexpath.row]
            }
        }
    }
    

    // MARK: - UI Event
    @IBAction func addLessonButtonDidPressed(sender: UIButton) {
        
        let actionController = UIAlertController(title: "Lesson Adding", message: "Choose a name for lesson (must be not existed)", preferredStyle: UIAlertControllerStyle.Alert)
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        let submit = UIAlertAction(title: "Submit", style: UIAlertActionStyle.Default) { [weak self] (action) -> Void in
            if let name = actionController.textFields?.first?.text {
                do {
                    let lesson = try LibraryAPI.sharedInstance.addLesson(name)
                    
                    self?.lessons = LibraryAPI.sharedInstance.getLessons()
                    
                    if let index = self?.lessons.indexOf(lesson) {
                        let addedIndexPath = NSIndexPath(forRow: index, inSection: 0)
                        self?.tableview.insertRowsAtIndexPaths([addedIndexPath], withRowAnimation: UITableViewRowAnimation.Right)
                    } else {
                        self?.tableview.reloadData()
                    }
                    
                } catch PersistencyManagerError.LessonNameCollision {
                    self?.showError(PersistencyManagerError.LessonNameCollision.description)
                } catch {
                    self?.showError(PersistencyManagerError.UnknowError.description)
                }
            }
        }
        actionController.addAction(submit)
        actionController.addAction(cancel)
        actionController.addTextFieldWithConfigurationHandler(nil)
        
        presentViewController(actionController, animated: true, completion: nil)
    }
    
    @IBAction func leftNavigationButtonDidPressed(sender: AnyObject) {}
    
    
    func longPress(gesture: UILongPressGestureRecognizer) {
        let location = gesture.locationInView(tableview)
        selectedIndexPath = tableview.indexPathForRowAtPoint(location)
        
        if selectedIndexPath != nil && gesture.state == UIGestureRecognizerState.Began {
            let alert = UIAlertController(title: "LESSON", message: "Choose a option", preferredStyle: UIAlertControllerStyle.ActionSheet)
            let delete = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                // delete lesson
                if let indexpath = self.selectedIndexPath {
                    let lesson = self.lessons.removeAtIndex(indexpath.row)
                    LibraryAPI.sharedInstance.deleteLesson(lesson)
                    self.tableview.deleteRowsAtIndexPaths([indexpath], withRowAnimation: UITableViewRowAnimation.Left)
                    self.selectedIndexPath = nil
                }
            }
            let edit = UIAlertAction(title: "Edit", style: UIAlertActionStyle.Default) { (action) -> Void in
                // TODO: edit lesson
                self.selectedIndexPath = nil
            }
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
            
            alert.addAction(delete)
            alert.addAction(edit)
            alert.addAction(cancel)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
}


// MARK: - UITableViewDataSource
extension LessonViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lessons.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LessonCellv2", forIndexPath: indexPath) as! LessonCell
        // cell configure
        cell.lesson = lessons[indexPath.row]
        return cell
    }
}


// MARK: - UITableViewDelegate
extension LessonViewController : UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .Normal, title: "Delete") { action, index in
            let lesson = self.lessons.removeAtIndex(indexPath.row)
            LibraryAPI.sharedInstance.deleteLesson(lesson)
            self.tableview.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        delete.backgroundColor = UIColor.redColor()
        
        let edit = UITableViewRowAction(style: .Normal, title: "Edit") { action, index in
            print("Edit button tapped")
        }
        edit.backgroundColor = UIColor(red: 84/255.0, green: 173/255.0, blue: 240/255.0, alpha: 1)
        
        return [delete, edit]
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
}




















