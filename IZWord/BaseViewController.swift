//
//  BaseViewController.swift
//  IZWord
//
//  Created by lvmtam on 9/15/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import UIKit

class BaseViewController : UIViewController {
    
    func showError(message: String) {
        showMessage("Error", message: message)
    }
    
    func showMessage(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
}
