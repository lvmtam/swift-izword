//
//  Theme.swift
//  IZWord
//
//  Created by lvmtam on 9/12/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import Foundation
import UIKit


enum Theme : Int {
    case Default
    case Dark
}



let SelectedThemeKey = "SelectedThemeKey"

struct ThemeManager {
    
    static func currentTheme() -> Theme {
        if let storedTheme = NSUserDefaults.standardUserDefaults().valueForKey(SelectedThemeKey)?.integerValue {
            return Theme(rawValue:storedTheme)!
        } else {
            return Theme.Default
        }
    }
    
    static func applyTheme(theme: Theme) {
        NSUserDefaults.standardUserDefaults().setValue(theme.rawValue, forKey: SelectedThemeKey)
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        // Navigation title configure
        if let titleFont = UIFont(name: "AvenirNext-Medium", size: 18) {
            UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName : titleFont,
                                                                NSForegroundColorAttributeName : UIColor.darkTextColor()];
        }
        
        // Navigation background configure
        UINavigationBar.appearance().barTintColor = UIColor.whiteColor()
        UINavigationBar.appearance().backgroundColor = UIColor.clearColor()
        UINavigationBar.appearance().shadowImage = UIImage()
        
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        delegate.window?.tintColor = UIColor.orangeColor()
    }
}