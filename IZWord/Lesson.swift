//
//  Lesson.swift
//  IZWord
//
//  Created by lvmtam on 9/14/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import Foundation
import CoreData

class Lesson: NSManagedObject {

    convenience init(name: String, managedObjectContext: NSManagedObjectContext) {
        self.init(context: managedObjectContext)
        self.name = name
    }
    
    
    class func lessonWithInfo(info: NSDictionary, managedObjectContext: NSManagedObjectContext) -> Lesson? {
        var lesson: Lesson?
        let name = info["name"] as! String
        let cards = info["cards"] as! NSArray
        let request = NSFetchRequest(entityName: "Lesson")
        request.predicate = NSPredicate(format: "name = %@", name)
        
        do {
            let matched = try managedObjectContext.executeFetchRequest(request)
            
            switch matched.count {
            case 0:
                lesson = Lesson(context: managedObjectContext)
                lesson?.name = name
                lesson?.addCards(Card.cardsFromArray(cards, managedObjectContext: managedObjectContext))
            case 1:
                lesson = matched.first as? Lesson
            default:
                print("fatal : have more 2 lesson have the same name")
            }
        } catch {
            print("fatal : can not fetch lesson \(name)")
        }
        
        return lesson
    }
    
    
    
    func addCard(card: Card) {
        if let storedCards = cards?.mutableCopy() as? NSMutableOrderedSet {
            storedCards.addObject(card)
            cards = storedCards
        }
    }
    
    func addCards(cards: [Card]) {
        for card in cards {
            addCard(card)
        }
    }

}
