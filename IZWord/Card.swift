//
//  Card.swift
//  IZWord
//
//  Created by lvmtam on 9/14/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import Foundation
import CoreData

class Card: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    
    convenience init(kanji: String, meaning: String, romaji: String?, context: NSManagedObjectContext) {
        self.init(context: context)
        self.kanji = kanji
        self.romaji = romaji
        self.meaning = meaning
    }
    
    class func cardWithInfo(info: NSDictionary, context: NSManagedObjectContext) -> Card? {
        var card: Card?
        let kanji = info["kanji"] as! String
        let romaji = info["romaji"] as! String
        let meaning = info["meaning"] as! String
        let request = NSFetchRequest(entityName: "Card")
        request.predicate = NSPredicate(format: "kanji = %@", kanji)
        
        do {
            let matched = try context.executeFetchRequest(request)
            
            switch matched.count {
            case 0:
                card = Card(kanji: kanji, meaning: meaning, romaji: romaji, context: context)
            case 1:
                card = matched.first as? Card
            default:
                print("fatal : have more 2 card have the same name")
            }
        } catch {
            print("fatal : can not fetch card \(kanji)")
        }
        return card
    }
    
    class func cardsFromArray(infos: NSArray, managedObjectContext: NSManagedObjectContext) -> [Card] {
        var cards = [Card]()
        for card in infos {
            if let dictionary = card as? NSDictionary {
                if let card = Card.cardWithInfo(dictionary, context: managedObjectContext) {
                    cards.append(card)
                }
            }
        }
        return cards
    }

}
