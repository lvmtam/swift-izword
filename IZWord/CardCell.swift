//
//  CardCell.swift
//  IZWord
//
//  Created by lvmtam on 9/13/15.
//  Copyright © 2015 lvmtam. All rights reserved.
//

import UIKit

class CardCell : UICollectionViewCell {
    
    @IBOutlet weak var lblKanji: UILabel!
    @IBOutlet weak var lblMeaning: UILabel!
    @IBOutlet weak var lblRomaji: UILabel!
    
    var card: Card? {
        didSet {
            if let kanji = card?.kanji {
                lblKanji.text = kanji
            }
            if let meaning = card?.meaning {
                lblMeaning.text = meaning
            }
            if let romaji = card?.romaji {
                lblRomaji.text = romaji
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = false
        layer.cornerRadius = 5;
        layer.shadowOffset = CGSizeMake(0, 0);
        layer.shadowRadius = 5;
        layer.shadowOpacity = 0.35;
        
        lblKanji.adjustsFontSizeToFitWidth = true
    }
}